import { createAppContainer, createStackNavigator } from "react-navigation";
import ListScreen from "../screens/list";
import NewScreen from "../screens/new";

const MainNavigator = createStackNavigator(
  {
    Home: ListScreen,
    New: NewScreen
  },
  {
    initialRouteName: "Home",
    headerMode: "none"
  }
);

const AppNavigator = createAppContainer(MainNavigator);

export default AppNavigator;
