import { ApolloClient, HttpLink, InMemoryCache } from "apollo-boost";

export const client = new ApolloClient({
  link: new HttpLink({
    uri:
      "https://us1.prisma.sh/public-luckox-377/reservation-graphql-backend/dev"
  }),
  cache: new InMemoryCache()
});
