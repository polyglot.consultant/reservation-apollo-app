import gql from "graphql-tag";

export const allQuery = gql`
  query {
    reservations {
      id
      name
      hotelName
      arrivalDate
      departureDate
    }
  }
`;
