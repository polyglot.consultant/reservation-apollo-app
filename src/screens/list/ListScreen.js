import React from "react";
import {
  Container,
  Header,
  Content,
  Text,
  Spinner,
  Body,
  Title,
  Fab,
  Icon,
  View
} from "native-base";
import { graphql } from "react-apollo";
import { allQuery } from "../../queries/reservations";
import ReservationsList from "./ReservationList";
import { Button } from "native-base";

class ListScreen extends React.Component {
  render() {
    const { loading, reservations } = this.props.data;

    return (
      <Container>
        <Header>
          <Body>
            <Title>Reservations</Title>
          </Body>
        </Header>
        <Content>
          {loading ? <Spinner /> : <ReservationsList data={reservations} />}
        </Content>
        <Fab
          position="bottomRight"
          onPress={() => this.props.navigation.push("New")}
        >
          <Icon name="add" />
        </Fab>
      </Container>
    );
  }
}

export default graphql(allQuery)(ListScreen);
