import React from "react";
import { Text, Icon, View, Card, CardItem } from "native-base";
import { withNavigation } from "react-navigation";
class ReservationsList extends React.Component {
  render() {
    const { data, navigation } = this.props;
    return (
      <View>
        {data.map(item => (
          <Card key={item.id}>
            <CardItem>
              <Icon active name="ios-person" />
              <Text>{item.name}</Text>
            </CardItem>
            <CardItem>
              <Icon active name="home" />
              <Text>{item.hotelName}</Text>
            </CardItem>
            <CardItem>
              <Icon active name="calendar" />
              <Text>Arrival: {item.arrivalDate}</Text>
            </CardItem>
            <CardItem>
              <Icon active name="calendar" />
              <Text>Departure: {item.departureDate}</Text>
            </CardItem>
          </Card>
        ))}
      </View>
    );
  }
}

export default withNavigation(ReservationsList);
