import React from "react";
import {
  Container,
  Header,
  Content,
  Text,
  Body,
  Title,
  Left,
  Button,
  Icon,
  Form,
  Item,
  Label,
  Input,
  DatePicker
} from "native-base";
import { graphql } from "react-apollo";
import { add } from "../../queries/reservations";

class NewScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      hotelName: "",
      arrivalDate: new Date().toLocaleDateString("en-US"),
      departureDate: new Date().toLocaleDateString("en-US")
    };
  }

  setDate = (key, date) => {
    this.setState({ [key]: date.toLocaleDateString("en-US") });
  };

  submit = () => {
    const { mutate } = this.props;
    mutate({ variables: { ...this.state } })
      .then(res => {
        alert("Reserved successfully!");
      })
      .catch(err => {
        alert("Reservation failed!. Please contact admin.");
      });
  };
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>New Reservation</Title>
          </Body>
        </Header>
        <Content padder>
          <Form>
            <Item>
              <Icon active name="ios-person" />
              <Input
                placeholder="Enter Name"
                onChangeText={text => this.setState({ name: text })}
              />
            </Item>
            <Item>
              <Icon active name="home" />
              <Input
                placeholder="Enter Hotel Name"
                onChangeText={text => this.setState({ hotelName: text })}
              />
            </Item>
            <Item>
              <Icon active name="calendar" />
              <DatePicker
                modalTransparent={false}
                animationType={"fade"}
                androidMode={"default"}
                placeHolderText="Enter Arrival Date"
                placeHolderTextStyle={{ color: "#d3d3d3" }}
                onDateChange={date => this.setDate("arrivalDate", date)}
              />
            </Item>
            <Item>
              <Icon active name="calendar" />
              <DatePicker
                modalTransparent={false}
                animationType={"fade"}
                androidMode={"default"}
                placeHolderText="Enter Departure Date"
                placeHolderTextStyle={{ color: "#d3d3d3" }}
                onDateChange={date => this.setDate("departureDate", date)}
              />
            </Item>
            <Button block onPress={() => this.submit()}>
              <Text>Reserve</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

export default graphql(add)(NewScreen);
